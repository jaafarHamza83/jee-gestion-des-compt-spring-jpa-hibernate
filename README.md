
Projet JEE Gestion des Comptes avec Spring, JPA, Hibernate :
==
Une formation sur youtube chaîne mohamedYoussfi.   

Sujet:
--
Développer une application web pour gérer les comptes bancaire.  
Deux types de comptes sont gérés: compte courant et compte d'épargne.  
Chaque compte subit plusieurs opérations de versement ou de retrait.  

Technologie utiliser: 
--
Spring Boot, Spring Web, Spring JPA,Spring Security, Hibernate, Thymeleaf, Bootstrap 3