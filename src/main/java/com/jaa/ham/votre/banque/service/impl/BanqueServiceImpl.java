package com.jaa.ham.votre.banque.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jaa.ham.votre.banque.dao.CompteRepository;
import com.jaa.ham.votre.banque.dao.OperationRepository;
import com.jaa.ham.votre.banque.entities.Compte;
import com.jaa.ham.votre.banque.entities.CompteCourant;
import com.jaa.ham.votre.banque.entities.Operation;
import com.jaa.ham.votre.banque.entities.Retrait;
import com.jaa.ham.votre.banque.entities.Versement;
import com.jaa.ham.votre.banque.service.IBanqueService;

@Service
@Transactional
public class BanqueServiceImpl implements IBanqueService {

	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private OperationRepository operationRepository;

	@Override
	public Compte consulter(String codeCpte) {
		Compte cpte = compteRepository.findOne(codeCpte);
		if (cpte == null) {
			throw new RuntimeException("Compte introuvable");
		}
		return cpte;
	}

	@Override
	public void verser(String codeCpte, double montant) {
		Compte cpte = consulter(codeCpte);
		Versement versement = new Versement(new Date(), montant, cpte);
		operationRepository.save(versement);
		cpte.setSolde(montant + cpte.getSolde());
		compteRepository.save(cpte);
	}

	@Override
	public void retirer(String codeCpte, double montant) {
		
		double facilitesCaisse = 0.0;
		Compte cpte = consulter(codeCpte);
		if (cpte instanceof CompteCourant) {
			facilitesCaisse = ((CompteCourant)cpte).getDecouvert();
		}
		if(cpte.getSolde()+facilitesCaisse<montant) {
			throw new RuntimeException("Solde insuffisant");
		}
		Retrait retrait = new Retrait(new Date(), montant, cpte);
		operationRepository.save(retrait);
		cpte.setSolde(cpte.getSolde() - montant);
		compteRepository.save(cpte);

	}

	@Override
	public void virement(String codeCpte1, String codeCpte2, double montant) {
		if(codeCpte1.equals(codeCpte2)) {
			throw new RuntimeException("Même numéro de compte. Opération impossible");
		}
		retirer(codeCpte1, montant);
		verser(codeCpte2, montant);
	}

	@Override
	public Page<Operation> listOperation(String codeCpte, int page, int size) {
		
		return operationRepository.listOperation(codeCpte, new PageRequest(page, size));
	}

}
