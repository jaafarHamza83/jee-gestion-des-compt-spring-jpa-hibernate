package com.jaa.ham.votre.banque.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jaa.ham.votre.banque.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
