package com.jaa.ham.votre.banque.service;

import org.springframework.data.domain.Page;

import com.jaa.ham.votre.banque.entities.Compte;
import com.jaa.ham.votre.banque.entities.Operation;

public interface IBanqueService {
	
	public Compte consulter(String codeCpte);
	public void verser(String codeCpte, double montant);
	public void retirer(String codeCpte, double montant);
	public void virement(String codeCpte1, String codeCpte2, double montant);
	public Page<Operation> listOperation(String codeCpte, int page, int size);
	

}
