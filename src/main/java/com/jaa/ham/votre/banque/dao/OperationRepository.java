package com.jaa.ham.votre.banque.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jaa.ham.votre.banque.entities.Operation;

public interface OperationRepository extends JpaRepository<Operation, Long> {
	@Query("select o from Operation o where o.compte.codeCompte=:codeCompte order by o.dateOperation desc,o.numero desc")
	public Page<Operation> listOperation(@Param("codeCompte") String codeCpte, Pageable pageable); 
}
