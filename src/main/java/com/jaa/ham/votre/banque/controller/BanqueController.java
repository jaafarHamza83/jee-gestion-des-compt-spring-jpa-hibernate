package com.jaa.ham.votre.banque.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jaa.ham.votre.banque.entities.Compte;
import com.jaa.ham.votre.banque.entities.Operation;
import com.jaa.ham.votre.banque.service.IBanqueService;

@Controller
public class BanqueController {

	@Autowired
	private IBanqueService iBanqueService;
	
	@RequestMapping("/operations")
	public String index() {
		return "comptes";
	}
	
	@RequestMapping("/consulterCompte")
	public String consulterCompte(Model model, String codeCompte,
			@RequestParam(name="page",defaultValue = "0")int page, 
			@RequestParam(name="size",defaultValue = "5")int size) {
		Compte compte=null;
		model.addAttribute("codeCompte", codeCompte);
		try {
			compte = iBanqueService.consulter(codeCompte);
			Page<Operation> pageOperations = iBanqueService.listOperation(codeCompte, page, size);
			int [] pages = new int [pageOperations.getTotalPages()];
			model.addAttribute("pages",pages);
			model.addAttribute("listOperations", pageOperations.getContent());
			model.addAttribute("compte", compte);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("exception", "Code compte must not be empty!");
		}
		
		return "comptes";
	}
	
	@RequestMapping(value = "/saveOperation", method=RequestMethod.POST)
	public String saveOperation(Model model, 
			String typeOperation, String codeCompte, 
			double montant, String codeCompte2) {
		
		try {
			if(typeOperation.equals("VERS")) {
				iBanqueService.verser(codeCompte, montant);
			}else if(typeOperation.equals("RET")) {
				iBanqueService.retirer(codeCompte, montant);
			}else if(typeOperation.equals("VIR")) {
				iBanqueService.virement(codeCompte, codeCompte2, montant);
			}
		} catch (Exception e) {
			model.addAttribute("error",e);
			return "redirect:/consulterCompte?codeCompte="+codeCompte+"&error="+e.getMessage();
		}
		
		return "redirect:/consulterCompte?codeCompte="+codeCompte;
	}
	
}
