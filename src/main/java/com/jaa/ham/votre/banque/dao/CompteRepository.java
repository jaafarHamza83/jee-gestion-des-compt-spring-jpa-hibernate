package com.jaa.ham.votre.banque.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jaa.ham.votre.banque.entities.Compte;

public interface CompteRepository extends JpaRepository<Compte, String> {

}
