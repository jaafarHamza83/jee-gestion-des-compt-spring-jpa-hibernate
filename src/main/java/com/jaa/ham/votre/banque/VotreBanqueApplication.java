package com.jaa.ham.votre.banque;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jaa.ham.votre.banque.dao.ClientRepository;
import com.jaa.ham.votre.banque.dao.CompteRepository;
import com.jaa.ham.votre.banque.dao.OperationRepository;
import com.jaa.ham.votre.banque.entities.Client;
import com.jaa.ham.votre.banque.entities.Compte;
import com.jaa.ham.votre.banque.entities.CompteCourant;
import com.jaa.ham.votre.banque.entities.CompteEpargne;
import com.jaa.ham.votre.banque.entities.Retrait;
import com.jaa.ham.votre.banque.entities.Versement;
import com.jaa.ham.votre.banque.service.IBanqueService;

@SpringBootApplication
public class VotreBanqueApplication implements CommandLineRunner {

	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private OperationRepository operationRepository;
	
	@Autowired
	private IBanqueService iBanqueService;
	
	public static void main(String[] args) {
		SpringApplication.run(VotreBanqueApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		
//		Client c1 = clientRepository.save(new Client("Jaafar Hamza", "jaafar.hamza83@gmail.com"));
//		Client c2 = clientRepository.save(new Client("Hamza Hamza", "hamza.hamza56@gmail.com"));
//		
//		Compte cpt1 = compteRepository.save(new CompteCourant("Cpt1", new Date(), 1000.00, c1, 600));
//		Compte cpt2 = compteRepository.save(new CompteEpargne("Cpt2", new Date(), 6000.00, c2, 5.5));
//		
//		operationRepository.save(new Versement(new Date(), 100.00, cpt1));
//		operationRepository.save(new Retrait(new Date(), 200.00, cpt2));
//		
//		iBanqueService.verser(cpt1.getCodeCompte(), 123000.00);
	}

}
